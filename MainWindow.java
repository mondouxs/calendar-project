package Calendar;

import com.mindfusion.common.DateTime;
import com.mindfusion.common.Duration;
import com.mindfusion.scheduling.Calendar;
import com.mindfusion.scheduling.CalendarView;
import com.mindfusion.scheduling.model.Appointment;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

/**
 * MainWindow is the window that retains information about, and displays
 * the calendar
 */
public class MainWindow extends JFrame implements ActionListener
{
    /**
     * @param Calendar the instance of the calendar that is being displayed
     *                 and edited
     */
    Calendar calendarTest = new Calendar();

    /**
     * creates a appointment with a selected event
     */
    public void createAppointment() {

    }


    /**
     * creates the calendar GUI that the user interacts with
     */
    protected MainWindow()
    {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(1000, 800);
        setTitle("CIS 350 Calendar View");
        getContentPane().add(calendarTest, BorderLayout.CENTER);
        calendarTest.beginInit();
        calendarTest.setCurrentView(CalendarView.Timetable);
        for (int i = 1; i <= 6; i++)
            calendarTest.getTimetableSettings().getDates().add(DateTime.today().addDays(i));
        calendarTest.getTimetableSettings().setVisibleColumns(7);

        calendarTest.endInit();



        JButton createNewAppButton = new JButton("Create new appointment");
        getContentPane().add(createNewAppButton, BorderLayout.BEFORE_FIRST_LINE);
        createNewAppButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                if (actionEvent.getSource() == createNewAppButton) {
                    Appointment app = new Appointment();
                    eventMenu menu = new eventMenu();
                    menu.event(app);
                    calendarTest.getSchedule().getItems().add(app);
                }
            }
        });

        //todo: create a new user Profile anda button that calls the createProfile function
        JButton weekView = new JButton("Week View");
        getContentPane().add(weekView, BorderLayout.WEST);
        weekView.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                if (actionEvent.getSource() == weekView) {
                    calendarTest.setCurrentView(CalendarView.Timetable);
                }
            }
        });

        JButton monthView = new JButton("Monthly View");
        getContentPane().add(monthView, BorderLayout.LINE_END);
        monthView.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                if (actionEvent.getSource() == monthView) {
                    calendarTest.setCurrentView(CalendarView.SingleMonth);
                }
            }
        });

        //Save function still in progress. I'll add the rest when it's working properly
        JMenuBar menuBar;
        JMenu file;
        JMenuItem menuItem;

        menuBar = new JMenuBar();
        setJMenuBar(menuBar);
        file = new JMenu("File");
        menuBar.add(file);
        menuItem = new JMenuItem("Save");
        file.add(menuItem);

    }


    private static final long serialVersionUID = 1L;

    @Override
    public void actionPerformed(ActionEvent actionEvent) {

    }
}
