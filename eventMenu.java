package Calendar;

import com.mindfusion.common.DateTime;
import com.mindfusion.scheduling.model.Appointment;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * eventMenu opens up a menu for users to create and set data on events to put on their
 * calendar. Information is collected and sent back to the main window
 */
public class eventMenu {
    /**
     * @param months an array containing strings of each month's number
     */
    String months[] = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"};
    /**
     * @param hours an array containing strings of each hour's number
     */
    String hours[] = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12",
            "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24"};

    /**
     * the function that opens the event menu. Users input data and it returns the selected
     * values to app
     * @param app the appointment that is being created
     */
    public void event(Appointment app){
        JFrame appFrame = new JFrame("New Event");
        //Setting the width and height of frame
        appFrame.setSize(350, 200);
        JPanel panel = new JPanel();
        appFrame.add(panel);
        appFrame.setVisible(true);

        panel.setLayout(null);
        JLabel eventName = new JLabel("Event Title: ");
        eventName.setBounds(10,20,80,25);
        panel.add(eventName);
        JTextField eventNameText = new JTextField(20);
        eventNameText.setBounds(140,20,165,25);
        panel.add(eventNameText);

        panel.setLayout(null);
        JLabel startDate = new JLabel("Start Date (Y/M/D/H): ");
        startDate.setBounds(10,50,120,25);
        panel.add(startDate);
        JTextField startDateYear = new JTextField(20);
        startDateYear.setBounds(140,50,60,25);
        startDateYear.setText("2020");
        panel.add(startDateYear);
        JComboBox startMonth = new JComboBox(months);
        startMonth.setBounds(200,50,45,25);
        panel.add(startMonth);
        JTextField startDateDay = new JTextField(20);
        startDateDay.setBounds(245,50,40,25);
        panel.add(startDateDay);
        JComboBox startHour = new JComboBox(hours);
        startHour.setBounds(285,50,40,25);
        panel.add(startHour);

        panel.setLayout(null);
        JLabel endDate = new JLabel("End Date (Y/M/D/H): ");
        endDate.setBounds(10,80,120,25);
        panel.add(endDate);
        JTextField endDateYear = new JTextField(20);
        endDateYear.setBounds(140,80,60,25);
        endDateYear.setText("2020");
        panel.add(endDateYear);
        JComboBox endMonth = new JComboBox(months);
        endMonth.setBounds(200,80,45,25);
        panel.add(endMonth);
        JTextField endDateDay = new JTextField(20);
        endDateDay.setBounds(245,80,40,25);
        panel.add(endDateDay);
        JComboBox endHour = new JComboBox(hours);
        endHour.setBounds(285,80,40,25);
        panel.add(endHour);

        JButton loginButton = new JButton("Create Event");
        loginButton.setBounds(75, 110, 200, 25);
        panel.add(loginButton);

        loginButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (actionEvent.getSource() == loginButton) {

                    app.setHeaderText(eventNameText.getText());
                    app.setStartTime(new DateTime(Integer.parseInt(startDateYear.getText()), Integer.parseInt((String) startMonth.getSelectedItem()), Integer.parseInt(startDateDay.getText()), Integer.parseInt((String)startHour.getSelectedItem()), 0, 0));
                    app.setEndTime(new DateTime(Integer.parseInt(endDateYear.getText()), Integer.parseInt((String)endMonth.getSelectedItem()), Integer.parseInt(endDateDay.getText()), Integer.parseInt((String)endHour.getSelectedItem()), 0, 0));
                    appFrame.setVisible(false);
                }
            }

        });
    }
}


