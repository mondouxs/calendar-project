package Calendar;

import javax.swing.*;

/**
 * runCalendar is the class used to run the program as a whole
 */
public class runCalendar {
    /**
     * runs the program
     *
     * @param args
     */
    public static void main(String[] args)
    {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                try {
                    new MainWindow().setVisible(true);
                }
                catch (Exception exp) {
                }
            }
        });
    }
}
